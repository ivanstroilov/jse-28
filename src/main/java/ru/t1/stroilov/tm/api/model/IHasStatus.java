package ru.t1.stroilov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
