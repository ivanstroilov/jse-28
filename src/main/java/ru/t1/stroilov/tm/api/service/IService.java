package ru.t1.stroilov.tm.api.service;

import ru.t1.stroilov.tm.api.repository.IRepository;
import ru.t1.stroilov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
