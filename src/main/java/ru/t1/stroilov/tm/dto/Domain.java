package ru.t1.stroilov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.model.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@XmlType(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonXmlRootElement(localName = "domain")
public final class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

    @NotNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JsonProperty("user")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> users = new ArrayList<>();

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JsonProperty("project")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JsonProperty("task")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks = new ArrayList<>();

}

