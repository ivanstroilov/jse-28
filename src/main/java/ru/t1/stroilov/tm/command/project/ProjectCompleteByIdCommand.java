package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Complete Project by ID.";

    @NotNull
    public final static String NAME = "project-complete-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(getUserId(), id, Status.COMPLETED);
    }
}
