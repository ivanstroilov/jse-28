package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;

public class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Delete all Projects.";

    @NotNull
    public final static String NAME = "project-delete";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECTS]");
        getProjectTaskService().removeProjects(getUserId());
        System.out.println("[PROJECTS DELETED]");
    }
}
