package ru.t1.stroilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String DESCRIPTION = "Delete Project by index.";

    @NotNull
    public final static String NAME = "project-delete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DELETE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = getProjectService().findByIndex(index);
        getProjectTaskService().removeProjectById(getUserId(), project.getId());
    }
}
