package ru.t1.stroilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.stroilov.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public final static String DESCRIPTION = "Logout User.";

    @NotNull
    public final static String NAME = "logout";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
